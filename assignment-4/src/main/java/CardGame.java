import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.io.File;
import java.util.concurrent.TimeUnit;


/** 
* Class to make main Programs (set button, action of the button) for the games
*/
public class CardGame{

    /** instrance variable for the main programs*/
    private int tries = 0;
    private JLabel label = new JLabel();
    private JPanel footPanel = new JPanel();
    private JPanel buttonPanel = new JPanel();
	private ArrayList<JToggleButton> listClick = new ArrayList<JToggleButton>();
	private ArrayList<JToggleButton> arrayButton = new ArrayList<JToggleButton>();
	private ArrayList<JToggleButton> buttonClicked = new ArrayList<JToggleButton>();

    /** Constructor of the Card Game */
    public CardGame() throws InterruptedException{
    	this.buttonPanel = buttonPanel;
    	this.footPanel = footPanel;
    	this.arrayButton = arrayButton;
    	this.tries = tries;
    	setButtons();
    	setButtonAction();
    	initFooterButtons();
    	SetLabelTries(0);
    }
    /**
    * Accesor to get the buttonPanel
    * @return button panel type of JPanel 
    */
    public JPanel getButtonPanel(){
    	return this.buttonPanel;
    }
    /**
    * Accesor to get the footerPanel
    * @return footer panel type of JPanel 
    */
    public JPanel getFooterPanel(){
    	return this.footPanel;
    }
    /** method to set the buttons, without no action */
	private void setButtons() throws InterruptedException{
		String path = "C://Users//Adi Surya//Documents//Documents//I Made Adi//UI//SEM 2//DDP 2//ProgFound2_Assignments//assignment-4//src//main//java//";
        File folder = new File(path + "Pic");
        File[] listGambar = folder.listFiles();
        ArrayList<File> arrGambar = new ArrayList<File>();

        buttonPanel.setLayout(new GridLayout(6,6));

        for (int i = 0;i < 6;i++){
            for(int j = 0; j < 6; j++){
                ImageIcon imageIcon = new ImageIcon(path + "Pic2//logo.png");
                Image image = imageIcon.getImage();
                Image setImage = image.getScaledInstance(80,80,java.awt.Image.SCALE_SMOOTH);
                ImageIcon finalImage = new ImageIcon(setImage);
                JToggleButton jb = new JToggleButton(finalImage);
                arrayButton.add(jb);
            }
        }

        for (int j = 0; j < listGambar.length; j++){
            ImageIcon imageIcon = new ImageIcon(""+listGambar[j]);
            Image image = imageIcon.getImage();
            Image setImage = image.getScaledInstance(80,80,java.awt.Image.SCALE_SMOOTH);
            ImageIcon finalImage = new ImageIcon(setImage);
            arrayButton.get(j*2).setSelectedIcon(finalImage);
            arrayButton.get(j*2+1).setSelectedIcon(finalImage);
        }
    }

    /** method to set action to each button registered */
    private void setButtonAction() throws InterruptedException{
        for (JToggleButton jtb : arrayButton){
            jtb.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    try{
                        if (listClick.size() == 0){
                            listClick.add(jtb);
                        }
                        else if (arrayButton.indexOf(jtb) != arrayButton.indexOf(listClick.get(0))){
                            listClick.add(jtb);
                            tries += 1;
                            SetLabelTries(tries);
                        }

                        if(listClick.size() == 2){
                            Thread.sleep(800);
                            if(listClick.get(0).getSelectedIcon().equals(listClick.get(1).getSelectedIcon())){
                                listClick.get(0).setVisible(false);
                                listClick.get(1).setVisible(false);
                                buttonClicked.add(listClick.get(0));
                                buttonClicked.add(listClick.get(1));
                                if(buttonClicked.size() == 36){
                                    finished();
                                }
                            }
                            else{
                                listClick.get(0).setSelected(false);
                                listClick.get(1).setSelected(false);
                            }
                            listClick = new ArrayList<JToggleButton>();
                        }
                    }
                    catch(InterruptedException ie){
                        Thread.currentThread().interrupt(); ;
                    }
                    
                }
            });
        }
        Collections.shuffle(arrayButton);

        for (JToggleButton button : arrayButton){
            buttonPanel.add(button);
        }
    }

    /** 
    * method to set Label (number of tries) to the footer panel
    * @param times to try type of Integer
    */
    private void SetLabelTries(int times){
    	String text = "Number of tries : " + String.valueOf(times);
        label.setText(text);
        label.setHorizontalAlignment(label.CENTER);
        footPanel.add(label,BorderLayout.PAGE_END);
    }

    /** method to initialize the footer button, include the action of the button*/
    private void initFooterButtons() throws InterruptedException{
    	footPanel.setLayout(new BorderLayout());

        JPanel footButton = new JPanel();
        footButton.setLayout(new FlowLayout());

        JButton playButton = new JButton("Play Again");
        playButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) { 
                try{
                    restartGame();
                } 
                catch(InterruptedException ie){
                    Thread.currentThread().interrupt(); ;
                }
            } 
        });
        footButton.add(playButton);

        JButton exitButton = new JButton("Exit");
        exitButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) { 
                System.exit(0);
            } 
        });
        footButton.add(exitButton);
        footPanel.add(footButton,BorderLayout.PAGE_START);
    }

    /** method to restart the game, if "Play Again" tombol clicked*/
    private void restartGame() throws InterruptedException{
    	SetLabelTries(0);
        for (JToggleButton jtb : arrayButton){
            jtb.setVisible(true);
            jtb.setSelected(false);
        }
        this.tries = 0;

        Collections.shuffle(arrayButton);

        for (JToggleButton button : arrayButton){
            buttonPanel.add(button);
        }
    }

    /** method which applied when the you finished the game, show a frame and a button Play Again */
    public void finished() throws InterruptedException { 
	    JFrame layar = new JFrame();
	    JPanel panel = new JPanel(); 

	    panel.setLayout(new BorderLayout());

	    JLabel label1 = new JLabel();
	    JLabel label2 = new JLabel();

	    label1.setText("Congratulation, you finished the game!");
	    label2.setText("You tried "+ this.tries + " times");
        this.tries = 0;

	    label1.setHorizontalAlignment(label1.CENTER);
	    label2.setHorizontalAlignment(label2.CENTER);

	    JPanel finishPanel = new JPanel();
        finishPanel.setLayout(new FlowLayout());

        JButton playButton = new JButton("Play Again");
        playButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try{
                    restartGame();
                    layar.setVisible(false);
                } 
                catch(InterruptedException ie){
                    Thread.currentThread().interrupt(); ;
                }
            } 
        });

        finishPanel.add(playButton);

	    panel.add(label1,BorderLayout.PAGE_START);
	    panel.add(label2,BorderLayout.CENTER);
	    panel.add(finishPanel,BorderLayout.PAGE_END);

	    layar.setContentPane(panel);
	    layar.pack();
	    layar.setVisible(true);
	} 
}