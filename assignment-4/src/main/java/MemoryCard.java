//import files
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.io.File;

/** 
* Class to make a Main Frame for the games
*/
public class MemoryCard {

    /** Instance Variable */
    private JPanel mainPanel;
    private int tries = 0;
    private CardGame cg = new CardGame();

    /** Constructor */
    public MemoryCard() { 
        JFrame frame = new JFrame("Remember the Character");
        mainPanel = new JPanel(); 
        mainPanel.setLayout(new BorderLayout()); 
        initButtons(); 
        initFooterButtons();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        ImageIcon image = new ImageIcon("C://Users//Adi Surya//Documents//Documents//I Made Adi//UI//SEM 2//DDP 2//ProgFound2_Assignments//assignment-4//src//main//java//Pic2//logo.png");
        frame.setIconImage(image.getImage());
        frame.setContentPane(mainPanel);
        frame.pack();
        frame.setVisible(true);
    } 

    /** Method init Buttions, to add the button to main frame */
    private void initButtons(){
        JPanel buttonPanel = cg.getButtonPanel();
        mainPanel.add(buttonPanel, BorderLayout.CENTER);
    }

    /** Method init Buttions, to add the footer button to main frame */
    private void initFooterButtons(){
        JPanel footerPanel = cg.getFooterPanel();
        mainPanel.add(footerPanel, BorderLayout.PAGE_END);
    }
}
