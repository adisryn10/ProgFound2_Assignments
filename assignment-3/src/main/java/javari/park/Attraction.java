package javari.park;

import java.util.List;

import javari.animal.Animal;

//membuat kelas atraksi yang mengimplements interace SelectedAttraction
public class Attraction implements SelectedAttraction{
	private String name;
	private String type;
	private List<Animal> performer;

	//Overloading digunakan untuk membuat atraksi (1 dengan parameter hewan, 2 tidak ada parameter)
	public Attraction(String name,String type, List<Animal> performer){
		this.name = name;
		this.type = type;
		this.performer = performer;
	}
	public Attraction(String name,String type){
		this.name = name;
		this.type = type;
		this.performer = performer;
	}
	public String getName(){
		return name;
	}
	public String getType(){
		return type;
	}
	public List<Animal> getPerformers(){
		return performer;
	}
}