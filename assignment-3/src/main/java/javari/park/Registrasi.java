package javari.park;

import java.util.List;
import javari.writer.*;

//Membuat elas registrasi yang mengimplement interface Registration
public class Registrasi implements Registration{
	private int id;
	private String name;
	private List<SelectedAttraction> atraksi;

	public Registrasi(int id,String name,List<SelectedAttraction> atraksi){
		this.id = id;
		this.name = name;
		this.atraksi = atraksi;
	}
	public int getRegistrationId(){
		return id;
	}

    public String getVisitorName(){
    	return name;
    }

    public String setVisitorName(String name){
    	return this.name = name;
    }
    //Membuat method getSelectedAtrraction
    public List<SelectedAttraction> getSelectedAttractions(){
    	return atraksi;
    }
    //Digunakan untuk menambah atraksi yang ingin ditonton oleh 1 orang
    public List<SelectedAttraction> addSelectedAttractions(SelectedAttraction atraksi){
    	this.atraksi.add(atraksi);
    	return this.atraksi;
    }
}