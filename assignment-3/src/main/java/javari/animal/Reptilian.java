package javari.animal;;

//membuat kelas reptile
public class Reptilian extends Animal{
	private String status;
	public Reptilian(Integer id, String type, String name, Gender gender, double length, double weight, String status,Condition condition){
		super(id,type,name,gender,length,weight,condition);
		this.status = status;
	}
	//cek spesifikasi reptil
	public boolean specificCondition(){
		if (status.equalsIgnoreCase("wild")){
			return false;
		}
		else{
			return true;
		}
	}
}