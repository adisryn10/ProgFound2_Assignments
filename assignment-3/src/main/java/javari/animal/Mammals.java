package javari.animal;
import javari.animal.Condition;

//Membuat kelas mammals 
public class Mammals extends Animal{
	private String pregnant;
	public Mammals(Integer id, String type, String name, Gender gender, double length, double weight, String pregnant,Condition condition){
		super(id,type,name,gender,length,weight,condition);
		this.pregnant = pregnant;
	}
	//Cek spesifikasi mamalia
	public boolean specificCondition(){
		if (pregnant.equalsIgnoreCase("pregnant")){
			return false;
		}
		else{
			return true;
		}
	}
} 