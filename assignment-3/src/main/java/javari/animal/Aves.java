package javari.animal;;

//membuat kelas aves
public class Aves extends Animal{
	private String layEggs;
	public Aves(Integer id, String type, String name, Gender gender, double length, double weight, String layEggs,Condition condition){
		super(id,type,name,gender,length,weight,condition);
		this.layEggs = layEggs;
	}
	//Cek spesifikasi aves
	public boolean specificCondition(){
		if (layEggs.equalsIgnoreCase("laying eggs")){
			return false;
		}
		else{
			return true;
		}
	}
}
