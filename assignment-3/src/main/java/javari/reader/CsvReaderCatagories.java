package javari.reader;

import java.nio.file.Path;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;

//Membuat kelas reader kcategories
public class CsvReaderCatagories extends CsvReader {
	private List<String> mammals = Arrays.asList("Hamster","Lion","Cat","Whale");
	private List<String> aves = Arrays.asList("Eagle","Parrot");
	private List<String> reptiles = Arrays.asList("Snake");
	private static int validCount;

	public CsvReaderCatagories(Path file) throws IOException{
		super(file);
	}
	public long countValidRecords(){
		int countValid = 0;
		int countMamal = 0;
		int countAves = 0;

		//Cek apakah tiap kategori terdiri dari jenis hewan yang tepat
		for (String barisan : lines){
			String[] baris = barisan.split(",");
			if (baris[2].equalsIgnoreCase("Explore the Mammals")){
				if(baris[1].equalsIgnoreCase("mammals")){
					countMamal += 1;
				}
				if(countMamal == 4){	//Jika semua mamals valid, maka valid bertambah 1
					countValid += 1;
				}
			}
			else if (baris[2].equalsIgnoreCase("World of Aves")){
				if(baris[1].equalsIgnoreCase("aves")){
					countAves += 1;
				}
				if(countAves == 2){
					countValid += 1;
				}
			}
			else if (baris[2].equalsIgnoreCase("Reptillian Kingdom")){
				if(baris[1].equalsIgnoreCase("reptiles")){
					countValid += 1;
				}
			}
		}
		validCount = countValid;
		return countValid;
	}
	public long countInvalidRecords(){
		int countInvalid = 0;
		
		return 3 - validCount;
	}
}