package javari.reader;

import java.nio.file.Path;                                                                                                                                                                                                                                                                                                                                                                       
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.List;
import javari.animal.*;
import java.util.ArrayList;
import javari.park.*;

//membuat kelas reader Atrraction
public class CsvReaderAttraction extends CsvReader {
	//Array berisi informasi atraksi yang bisa dilakukan oleh hewan-hewan
	private List<String> circleOfFire = Arrays.asList("Eagle","Lion","Whale");
	private List<String> dancingAnimals = Arrays.asList("Parrot","Cat","Hamster","Snake");
	private List<String> countingMasters = Arrays.asList("Hamster","Whale","Parrot");
	private List<String> passionateCoders = Arrays.asList("Hamster","Cat","Snake");

	//Untuk menyimpan seluruh atraksi
	public List<Attraction> listAtraksiHewan =   new ArrayList<Attraction>();

	//Untuk menyimmpan atraksi yang bisa dilakukan tiap hewan
	public List<Attraction> atraksiEagle = new ArrayList<Attraction>();
	public List<Attraction> atraksiParrot = new ArrayList<Attraction>();
	public List<Attraction> atraksiCat = new ArrayList<Attraction>();
	public List<Attraction> atraksiLion = new ArrayList<Attraction>();
	public List<Attraction> atraksiHamster = new ArrayList<Attraction>();
	public List<Attraction> atraksiWhale = new ArrayList<Attraction>();
	public List<Attraction> atraksiSnake = new ArrayList<Attraction>();

	//untuk menghitung invalid
	private static int validCount;

	public CsvReaderAttraction(Path file) throws IOException{
		super(file);
	}
	//menghitung valid
	public long countValidRecords(){
		int countValid = 0;
		int countCircle = 0;
		int countMaster = 0;
		int countDancing = 0;
		int countPassionate = 0;

		//Looping untuk mengecek vaidasi
		for (String barisan : lines){
			String[] baris = barisan.split(",");
			if (baris[1].equalsIgnoreCase("Circles of Fires")){
				if(circleOfFire.contains(baris[0])){
					countCircle += 1;
				}
				if(countCircle == 3){
					countValid += 1;
				}
			}
			else if (baris[1].equalsIgnoreCase("Dancing Animals")){
				if(dancingAnimals.contains(baris[0])){
					countDancing += 1;
				}
				if(countDancing == 4){
					countValid += 1;
				}
			}
			else if (baris[1].equalsIgnoreCase("Counting Masters")){
				if(countingMasters.contains(baris[0])){
					countMaster += 1;
				}
				if(countMaster == 3){
					countValid += 1;
				}
			}
			else if (baris[1].equalsIgnoreCase("Passionate Coders")){
				if(passionateCoders.contains(baris[0])){
					countPassionate += 1;
				}
				if(countPassionate == 3){
					countValid += 1;
				}
			}
		}
		//menambah seluruh atraksi hewan ke arraylist atraksi
		for (String barisan2 : lines){
			String[] baris2 = barisan2.split(",");
			Attraction aksi = new Attraction(baris2[1],baris2[0]);
			listAtraksiHewan.add(aksi);
		}
		//looping untuk menambah atraksi tiap hewan
		for (Attraction atraksi : listAtraksiHewan){
			if (atraksi.getType().equals("Eagle")){
				atraksiEagle.add(atraksi);
			}
			else if (atraksi.getType().equals("Parrot")){
				atraksiParrot.add(atraksi);
			}
			else if (atraksi.getType().equals("Cat")){
				atraksiCat.add(atraksi);
			}
			else if (atraksi.getType().equals("Hamster")){
				atraksiHamster.add(atraksi);
			}
			else if (atraksi.getType().equals("Lion")){
				atraksiLion.add(atraksi);
			}
			else if (atraksi.getType().equals("Whale")){
				atraksiWhale.add(atraksi);
			}
			else if (atraksi.getType().equals("Snake")){
				atraksiSnake.add(atraksi);
			}
		}
		validCount = countValid;
		return countValid;
	}
	public long countInvalidRecords(){
		return 4 - validCount;
		
	}
}