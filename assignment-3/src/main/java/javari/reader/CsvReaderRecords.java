package javari.reader;

import java.nio.file.Path;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import javari.animal.*;
import java.util.ArrayList;

//membuat kelas reader records
public class CsvReaderRecords extends CsvReader {
	//Array of mamals, aves dan reptiles (untuk cek validasi)
	public List<String> mammals = Arrays.asList("Hamster","Lion","Cat","Whale");
	public List<String> aves = Arrays.asList("Eagle","Parrot");
	public List<String> reptiles = Arrays.asList("Snake");

	//Array Setiap hewan, digunakan untuk menyimpan data tiap hewan yang bisa tampil
	public List<Animal> arrayEagle = new ArrayList<Animal>();
	public List<Animal> arrayParrot = new ArrayList<Animal>();
	public List<Animal> arrayCat = new ArrayList<Animal>();
	public List<Animal> arrayLion = new ArrayList<Animal>();
	public List<Animal> arrayWhale = new ArrayList<Animal>();
	public List<Animal> arrayHamster = new ArrayList<Animal>();
	public List<Animal> arraySnake = new ArrayList<Animal>();
	
	//valid count, untuk menghitung invalid
	private int validCount;

	public CsvReaderRecords(Path file) throws IOException{
		super(file);
	}
	
	public long countValidRecords(){
		int countValid = 0;
		for (String barisan : lines){
			String[] baris = barisan.split(",");
			int id = Integer.parseInt(baris[0]);
			String tipe = baris[1];
			String nama = baris[2];
			double length = Double.parseDouble(baris[4]);
			double weight = Double.parseDouble(baris[5]);
			String info = baris[6];
			Condition kondisi = Condition.parseCondition(baris[7]);
			Gender gender = Gender.parseGender(baris[3]);

			//Cek validasi tiap kingdom
			if (mammals.contains(baris[1]) && (baris[6].equals("") || baris[6].equals("pregnant"))){
				countValid += 1;
				//cek validasi tiap hewan
				if (baris[1].equals("Hamster")){												//Cek jenis hewan
					Mammals hams = new Mammals(id,tipe,nama,gender,length,weight,info,kondisi);	//Membuat objek hewan
					if(hams.isShowable()){														//Jika dapat tampil
						arrayHamster.add(hams);													//Array hewan ditambah objek hewan
					}
				}
				else if(baris[1].equals("Lion")){
					Mammals lion = new Mammals(id,tipe,nama,gender,length,weight,info,kondisi);
					if(lion.isShowable()){
						arrayLion.add(lion);
					}
				}
				else if(baris[1].equals("Cat")){
					Mammals cat = new Mammals(id,tipe,nama,gender,length,weight,info,kondisi);
					if(cat.isShowable()){
						arrayCat.add(cat);
					}
				}
				else if(baris[1].equals("Whale")){
					Mammals lumba = new Mammals(id,tipe,nama,gender,length,weight,info,kondisi);
					if(lumba.isShowable()){
						arrayWhale.add(lumba);
					}
				}
			}
			else if (aves.contains(baris[1]) && (baris[6].equals("") || baris[6].equals("layying eggs"))){
				countValid += 1;
				if (baris[1].equals("Eagle")){
					Aves elang = new Aves(id,tipe,nama,gender,length,weight,info,kondisi);
					if(elang.isShowable()){
						arrayEagle.add(elang);
					}	
				}
				else if(baris[1].equals("Parrot")){
					Aves beo = new Aves(id,tipe,nama,gender,length,weight,info,kondisi);
					if(beo.isShowable()){
						arrayParrot.add(beo);
					}
				}
			}
			else if (reptiles.contains(baris[1]) && (baris[6].equals("") || baris[6].equals("wild") || baris[6].equals("tame"))){
				countValid += 1;
				Reptilian ular = new Reptilian(id,tipe,nama,gender,length,weight,info,kondisi);
				if(ular.isShowable()){
					arraySnake.add(ular);
				}
			}
		}
		validCount = countValid;
		return countValid;
	}
	public long countInvalidRecords(){
		return lines.size() - validCount;
		
	}
}