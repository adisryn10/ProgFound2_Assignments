package javari.reader;

import java.nio.file.Path;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;
import javari.reader.*;

//Membuat kelas reader section
public class CsvReaderSection extends CsvReader {
	//Membuat array of mamals, aves, reptile. Digunakan untuk mengecek validasi
	private List<String> mammals = Arrays.asList("Hamster","Lion","Cat","Whale");
	private List<String> aves = Arrays.asList("Eagle","Parrot");
	private List<String> reptiles = Arrays.asList("Snake");

	//valid count digunakan untuk menghitung invalid
	private static int validCount;

	public CsvReaderSection(Path file) throws IOException{
		super(file);
	}
	//method valid
	public long countValidRecords(){
		int countValid = 0;
		int countMamal = 0;
		int countAves = 0;
		//Cek apakah tiap section terdiri dari jenis hewan yang tepat
		for (String barisan : lines){
			String[] baris = barisan.split(",");
			if (baris[2].equalsIgnoreCase("Explore the Mammals")){
				if(mammals.contains(baris[0])){
					countMamal += 1;
				}
				if(countMamal == 4){	//Jika semua mamals valid, maka valid bertambah 1
					countValid += 1;
				}
			}
			else if (baris[2].equalsIgnoreCase("World of Aves")){
				if(aves.contains(baris[0])){
					countAves += 1;
				}
				if(countAves == 2){
					countValid += 1;
				}
			}
			else if (baris[2].equalsIgnoreCase("Reptillian Kingdom")){
				if(reptiles.contains(baris[0])){
					countValid += 1;
				}
			}
		}
		validCount = countValid;
		return countValid;
	}
	//method tidakvalid
	public long countInvalidRecords(){
		int countInvalid = 0;
		
		return 3 - validCount;
	}
}