//Import module
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;
import javari.reader.*;
import javari.animal.*;
import javari.park.*;
import javari.writer.*;
import java.util.List;
import java.util.Arrays;
import java.util.ArrayList;
import org.json.JSONWriter;

//Make main class
public class A3Festival{
	public static void main(String[] args) throws IOException{
		//make input
		Scanner input = new Scanner(System.in);

		ArrayList<Registrasi> listRegistrasi = new ArrayList<Registrasi>();				//list registrasi berisi file file yang akan dicetak ke JSON
		List<SelectedAttraction> listAtraksi = new ArrayList<SelectedAttraction>();		//list atraksi berisi atraksi-atraksi yang akan ditonton oleh visitor

		//Cetak sesuai perintah
		System.out.println("Welcome to Javari Park Festival - Registration Service!\n");
		System.out.println("... Opening default section database from data. ... File not found or incorrect file!\n");	
		System.out.print("Please provide the source data path : ");						
		String data = input.nextLine();																						//Input datapath
		System.out.println("\n... Loading... Success... System is populating data...");

		Path file1 = Paths.get(data + "/Adi Surya/Documents/Documents/I Made Adi/UI/SEM 2/DDP 2/ProgFound2_Assignments/assignment-3/src/main/java/input data/animal_categories.csv"); //Merubah Tipe dat string menjadi path(Untuk csv reader dan JSON Writer)
		Path file2 = Paths.get(data + "/Adi Surya/Documents/Documents/I Made Adi/UI/SEM 2/DDP 2/ProgFound2_Assignments/assignment-3/src/main/java/input data/animal_attraction.csv");
		Path file3 = Paths.get(data + "/Adi Surya/Documents/Documents/I Made Adi/UI/SEM 2/DDP 2/ProgFound2_Assignments/assignment-3/src/main/java/input data/animal_records.csv");

		//Membuat object bertipe CSV reader yang berbeda dari tiap kelas
		CsvReaderSection sections = new CsvReaderSection(file1);
		CsvReaderCatagories catagories = new CsvReaderCatagories(file1);
		CsvReaderAttraction attraction = new CsvReaderAttraction(file2);
		CsvReaderRecords records = new CsvReaderRecords(file3);
		//Counting valid and invalid records
		System.out.println("\nFound " + sections.countValidRecords()   + " valid sections and "		+ sections.countInvalidRecords()   + " invalid sections"    + 
						   "\nFound " + attraction.countValidRecords() + " valid attractions and "	+ attraction.countInvalidRecords() + " invalid attractions" + 
						   "\nFound " + catagories.countValidRecords() + " valid categories and "	+ catagories.countInvalidRecords() + " invalid categories"  +
						   "\nFound " + records.countValidRecords()    + " valid records and "		+ records.countInvalidRecords()	   + " invalid records");

		System.out.println("\nWelcome to Javari Park Festival - Registration Service!");
		System.out.println("\nPlease answer the questions by typing the number. Type # if you want to return to the previous menu");
		//Array of animal(berisi array-array yang berisi tiap jenis hewan, diambil dari CsvrReaderRecords)
		List<Animal> arrayEagle = records.arrayEagle;
		List<Animal> arrayParrot = records.arrayParrot;
		List<Animal> arrayCat = records.arrayCat;
		List<Animal> arrayLion = records.arrayLion;
		List<Animal> arrayWhale = records.arrayWhale;
		List<Animal> arrayHamster = records.arrayHamster;
		List<Animal> arraySnake = records.arraySnake;

		//Array of attraksi (berisi array-array yang berisi atraksi tiap jenis hewan, diambil dari CsvrReaderAtraksi)
		List<Attraction> atraksiEagle = attraction.atraksiEagle;
		List<Attraction> atraksiParrot = attraction.atraksiParrot;
		List<Attraction> atraksiCat = attraction.atraksiCat;
		List<Attraction> atraksiLion = attraction.atraksiLion;
		List<Attraction> atraksiHamster = attraction.atraksiHamster;
		List<Attraction> atraksiWhale = attraction.atraksiWhale;
		List<Attraction> atraksiSnake = attraction.atraksiSnake;

		//Registrasi awal 0
		int registrationId = 0;
		//boolean loop1 ,loop2, loop3 dst berfungsi untuk mengembalikan ke menu sebelumnya, serta memberhentikan program atau mendaftar kembali
		boolean loop1 = true;
		//Loop1 (untuk memilih section)
		String name = "";
		while(loop1){
			boolean loop2 = true;
			System.out.println("\nJavari Park has 3 sections:\n" + 
							   "1. Explore the Mammals\n" + 
							   "2. World of Aves\n" + 
							   "3. Reptilian Kingdom");
			System.out.print("Please choose your preferred section (type the number): ");
			String numSection = input.next();

			//Cek tiap input(1 untuk Mamals 2 untuk Aves 3 untuk Reptil)
			if (numSection.equals("1")){
				//Loop2 (untuk memilih hewan-hewan dari tiap Kingdom)
				while(loop2){
					System.out.println("\n--Explore the Mammals--\n1.Hamster\n2.Lion\n3.Cat\n4.Whale\n");
					System.out.print("Please choose your preferred animals (type the number) :");
					boolean loop3 = true;
					String numAnimal = input.next();
					//Cek input(1 untuk hamster, 2 untuk Lion, 3 untuk Cat dan 4 untuk whale)
					if (numAnimal.equals("1")){
						//Cek apakah arrayHewan lebih dari 0 (ada yang bisa melakukan atraksi)
						if (arrayHamster.size() > 0){
							// LOOP 3(Untuk memilih atraksi dari tiap hewan)
							while(loop3){
								boolean loop4 = true;
								String atraksiHewanStr = "";
								//Looping untuk mendapat atraksi hewan(digunakan untuk mencetak atraksi-atraksi dari hewan yang ingin ditonton)
								for (Attraction eachAttraction : atraksiHamster){
									atraksiHewanStr += (atraksiHamster.indexOf(eachAttraction)+1) + ". " + eachAttraction.getName() + "\n";
								}
								System.out.println("\n---Hamster---\nAttractions by Hamster:\n" + atraksiHewanStr);
								System.out.print("Please choose your preferred attraction (type the number) :");
								String numAttraction = input.next();
								//Looping untuk mengecek numAtraksi ada dalam atraksiHewan atau tidak
								for (Attraction eachAttraction : atraksiHamster){
									//String untuk mengecek apakah atraksi tersedia
									//Karena index dimulai dari 0, maka index atraksi adalah index + 1
									String indexAtraksi = String.valueOf((atraksiHamster.indexOf(eachAttraction)+1));
									//Jika num atraksi sama dengan index atraksi
									if(numAttraction.equals(indexAtraksi)){
										//Jika ada, maka membuat objek atraksi
										Attraction atraksi = new Attraction(eachAttraction.getName(),eachAttraction.getType(),arrayHamster);
										while(loop4){
											boolean loop5 = true;
											//Meminta nama
											System.out.print("Wow, one more step, please let us know your name: ");
											input.nextLine();
											name = input.nextLine();
											//Found, digunakan untuk mengecek apakah nama sudah terdaftar sebelumnya. Jika sudah, atraksi bertambah
											//Berhenti saat registrasi sudah terdaftar, apabila tidak, list atraksi dibuat baru 
											found:{
												for (Registrasi regiseach : listRegistrasi){
													if (name.equals(regiseach.getVisitorName())){
														listAtraksi = regiseach.addSelectedAttractions(atraksi);
														name = regiseach.getVisitorName();
														registrationId = regiseach.getRegistrationId();
														break found;
													}
												}
												registrationId += 1;
												listAtraksi = new ArrayList<SelectedAttraction>();
												listAtraksi.add(atraksi);
											}
											if(name.equals("#")){
												loop4 = false;
											}
											else{
												//Loop untuk mendapat nama-nama performer (digunakan untuk validasi data)
												String namePerform = "";
												for (Animal performer : atraksi.getPerformers()){
													namePerform += performer.getName() + ",";
												}
												System.out.println("Yeay, final check!\nHere is your data, and the attraction you chose:"+
																   "\nName : " + name +
																   "\nAttractions : " + atraksi.getName() + " -> "+ atraksi.getType() +
																   "\nWith : " + namePerform);
												System.out.print("Is the data correct? (Y/N): ");
												String opsi = input.next();
												if (opsi.equals("Y")){
													while(loop5){
														//Jika sudah benar, file registrasi dibuat 
														Registrasi regis = new Registrasi(registrationId,name,listAtraksi);
														found:{
															for (Registrasi regisBefore : listRegistrasi){
																if (regisBefore.getVisitorName().equals(regis.getVisitorName())){
																	listRegistrasi.remove(regisBefore);
																	listRegistrasi.add(regis);
																	break found;
																}
															}
															listRegistrasi.add(regis);
														}
														System.out.print("Thank you for your interest. Would you like to register to other attractions? (Y/N): ");
														String opsi2 = input.next();
														//Cek untuk mendaftar kembali atau tidak
														if(opsi2.equals("N")){
															//Path untuk menaruh data
															Path directory = Paths.get("C:/Users/Adi Surya/Documents/Documents/I Made Adi/UI/SEM 2/DDP 2/ProgFound2_Assignments/assignment-3/src/main/java");
															//Loop untuk cetak data
															for (Registrasi regiseach : listRegistrasi){
																RegistrationWriter.writeJson(regiseach, directory);
															}
															loop1 = false;loop2= false;loop3=false;loop4=false;loop5=false;
														}
														else if(opsi2.equals("Y")){
															loop2 = false;loop3=false;loop4=false;loop5=false;
														}
														else if (opsi2.equals("#")){
															loop5 = false;
														}
													}
												}
												else if (opsi.equals("N")){
													loop4 = false;
												}
												else if (opsi.equals("#")){
													loop4 = false;
												}
											}
										}
									}
								}
								if (numAttraction.equals("#")){
									loop3 = false;
								}
							}
						}
						else{
							System.out.println("Unfortunately, no hamster can perform any attraction, please choose other animals");
						}
					}
					else if (numAnimal.equals("2")){
						if (arrayLion.size() > 0){
							while(loop3){
								boolean loop4 = true;
								String atraksiHewanStr = "";
								for (Attraction eachAttraction : atraksiLion){
									atraksiHewanStr += (atraksiLion.indexOf(eachAttraction)+1) + ". " + eachAttraction.getName() + "\n";
								}
								System.out.println("\n---Lion---\nAttractions by Lion:\n" + atraksiHewanStr);
								System.out.print("Please choose your preferred attraction (type the number) :");
								String numAttraction = input.next();
								for (Attraction eachAttraction : atraksiLion){
									String indexAtraksi = String.valueOf((atraksiLion.indexOf(eachAttraction)+1));
									if(numAttraction.equals(indexAtraksi)){
										Attraction atraksi = new Attraction(eachAttraction.getName(),eachAttraction.getType(),arrayLion);
										while(loop4){
											boolean loop5 = true;
											System.out.print("Wow, one more step, please let us know your name: ");
											input.nextLine();
											name = input.nextLine();
											found:{
												for (Registrasi regiseach : listRegistrasi){
													if (name.equals(regiseach.getVisitorName())){
														listAtraksi = regiseach.addSelectedAttractions(atraksi);
														name = regiseach.getVisitorName();
														registrationId = regiseach.getRegistrationId();
														break found;
													}
												}
												registrationId += 1;
												listAtraksi = new ArrayList<SelectedAttraction>();
												listAtraksi.add(atraksi);
											}
											if(name.equals("#")){
												loop4 = false;
											}
											else{
												String namePerform = "";
												for (Animal performer : atraksi.getPerformers()){
													namePerform += performer.getName() + ",";
												}
												System.out.println("Yeay, final check!\nHere is your data, and the attraction you chose:"+
																   "\nName : " + name +
																   "\nAttractions : " + atraksi.getName() + " -> "+ atraksi.getType() +
																   "\nWith : " + namePerform);
												System.out.print("Is the data correct? (Y/N): ");
												String opsi = input.next();
												if (opsi.equals("Y")){
													while(loop5){
														Registrasi regis = new Registrasi(registrationId,name,listAtraksi);
														found:{
															for (Registrasi regisBefore : listRegistrasi){
																if (regisBefore.getVisitorName().equals(regis.getVisitorName())){
																	listRegistrasi.remove(regisBefore);
																	listRegistrasi.add(regis);
																	break found;
																}
															}
															listRegistrasi.add(regis);
														}
														String opsi2 = input.next();
														if(opsi2.equals("N")){
															Path directory = Paths.get("C:/Users/Adi Surya/Documents/Documents/I Made Adi/UI/SEM 2/DDP 2/ProgFound2_Assignments/assignment-3/src/main/java");
															for (Registrasi regiseach : listRegistrasi){
																RegistrationWriter.writeJson(regiseach, directory);
															}
															loop1 = false;loop2= false;loop3=false;loop4=false;loop5=false;
														}
														else if(opsi2.equals("Y")){
															loop2 = false;loop3=false;loop4=false;loop5=false;
														}
														else if (opsi2.equals("#")){
															loop5 = false;
														}
													}
												}
												else if (opsi.equals("N")){
													loop4 = false;
												}
												else if (opsi.equals("#")){
													loop4 = false;
												}
											}
										}
									}
								}
								if (numAttraction.equals("#")){
									loop3 = false;
								}
							}
						}
						else{
							System.out.println("Unfortunately, no lion can perform any attraction, please choose other animals");
							
						}
					}
					else if (numAnimal.equals("3")){
						if (arrayCat.size() > 0){
							while(loop3){
								boolean loop4 = true;
								String atraksiHewanStr = "";
								for (Attraction eachAttraction : atraksiCat){
									atraksiHewanStr += (atraksiCat.indexOf(eachAttraction)+1) + ". " + eachAttraction.getName() + "\n";
								}
								System.out.println("\n---Cat---\nAttractions by Cat:\n" + atraksiHewanStr);
								System.out.print("Please choose your preferred attraction (type the number) :");
								String numAttraction = input.next();
								for (Attraction eachAttraction : atraksiCat){
									String indexAtraksi = String.valueOf((atraksiCat.indexOf(eachAttraction)+1));
									if(numAttraction.equals(indexAtraksi)){
										Attraction atraksi = new Attraction(eachAttraction.getName(),eachAttraction.getType(),arrayCat);
										while(loop4){
											boolean loop5 = true;
											System.out.print("Wow, one more step, please let us know your name: ");
											input.nextLine();
											name = input.nextLine();
											found:{
												for (Registrasi regiseach : listRegistrasi){
													if (name.equals(regiseach.getVisitorName())){
														listAtraksi = regiseach.addSelectedAttractions(atraksi);
														name = regiseach.getVisitorName();
														registrationId = regiseach.getRegistrationId();
														break found;
													}
												}
												registrationId += 1;
												listAtraksi = new ArrayList<SelectedAttraction>();
												listAtraksi.add(atraksi);
											}
											if(name.equals("#")){
												loop4 = false;
											}
											else{
												String namePerform = "";
												for (Animal performer : atraksi.getPerformers()){
													namePerform += performer.getName() + ",";
												}
												System.out.println("Yeay, final check!\nHere is your data, and the attraction you chose:"+
																   "\nName : " + name +
																   "\nAttractions : " + atraksi.getName() + " -> "+ atraksi.getType() +
																   "\nWith : " + namePerform);
												System.out.print("Is the data correct? (Y/N): ");
												String opsi = input.next();
												if (opsi.equals("Y")){
													while(loop5){
														System.out.print("Thank you for your interest. Would you like to register to other attractions? (Y/N): ");
														Registrasi regis = new Registrasi(registrationId,name,listAtraksi);
														found:{
															for (Registrasi regisBefore : listRegistrasi){
																if (regisBefore.getVisitorName().equals(regis.getVisitorName())){
																	listRegistrasi.remove(regisBefore);
																	listRegistrasi.add(regis);
																	break found;
																}
															}
															listRegistrasi.add(regis);
														}
														String opsi2 = input.next();
														if(opsi2.equals("N")){
															Path directory = Paths.get("C:/Users/Adi Surya/Documents/Documents/I Made Adi/UI/SEM 2/DDP 2/ProgFound2_Assignments/assignment-3/src/main/java");
															for (Registrasi regiseach : listRegistrasi){
																RegistrationWriter.writeJson(regiseach, directory);
															}
															loop1 = false;loop2= false;loop3=false;loop4=false;loop5=false;
														}
														else if(opsi2.equals("Y")){
															loop2 = false;loop3=false;loop4=false;loop5=false;
														}
														else if (opsi2.equals("#")){
															loop5 = false;
														}
													}
												}
												else if (opsi.equals("N")){
													loop4 = false;
												}
												else if (opsi.equals("#")){
													loop4 = false;
												}
											}
										}
									}
								}
								if (numAttraction.equals("#")){
									loop3 = false;
								}
							}
						}
						else{
							System.out.println("Unfortunately, no cat can perform any attraction, please choose other animals");
							
						}
					}
					else if (numAnimal.equals("4")){
						if (arrayWhale.size() > 0){
							while(loop3){
								boolean loop4 = true;
								String atraksiHewanStr = "";
								for (Attraction eachAttraction : atraksiWhale){
									atraksiHewanStr += (atraksiWhale.indexOf(eachAttraction)+1) + ". " + eachAttraction.getName() + "\n";
								}
								System.out.println("\n---Whale---\nAttractions by Whale:\n" + atraksiHewanStr);
								System.out.print("Please choose your preferred attraction (type the number) :");
								String numAttraction = input.next();
								for (Attraction eachAttraction : atraksiWhale){
									String indexAtraksi = String.valueOf((atraksiWhale.indexOf(eachAttraction)+1));
									if(numAttraction.equals(indexAtraksi)){
										Attraction atraksi = new Attraction(eachAttraction.getName(),"Whale",arrayWhale);
										while(loop4){
											boolean loop5 = true;
											System.out.print("Wow, one more step, please let us know your name: ");
											input.nextLine();
											name = input.nextLine();
											found:{
												for (Registrasi regiseach : listRegistrasi){
													if (name.equals(regiseach.getVisitorName())){
														listAtraksi = regiseach.addSelectedAttractions(atraksi);
														name = regiseach.getVisitorName();
														registrationId = regiseach.getRegistrationId();
														break found;
													}
												}
												registrationId += 1;
												listAtraksi = new ArrayList<SelectedAttraction>();
												listAtraksi.add(atraksi);
											}
											if(name.equals("#")){
												loop4 = false;
											}
											else{
												String namePerform = "";
												for (Animal performer : atraksi.getPerformers()){
													namePerform += performer.getName() + ",";
												}
												System.out.println("Yeay, final check!\nHere is your data, and the attraction you chose:"+
																   "\nName : " + name +
																   "\nAttractions : " + atraksi.getName() + " -> "+ atraksi.getType() +
																   "\nWith : " + namePerform);
												System.out.print("Is the data correct? (Y/N): ");
												String opsi = input.next();
												if (opsi.equals("Y")){
													while(loop5){
														System.out.print("Thank you for your interest. Would you like to register to other attractions? (Y/N): ");
														Registrasi regis = new Registrasi(registrationId,name,listAtraksi);
														found:{
															for (Registrasi regisBefore : listRegistrasi){
																if (regisBefore.getVisitorName().equals(regis.getVisitorName())){
																	listRegistrasi.remove(regisBefore);
																	listRegistrasi.add(regis);
																	break found;
																}
															}
															listRegistrasi.add(regis);
														}
														String opsi2 = input.next();
														if(opsi2.equals("N")){
															Path directory = Paths.get("C:/Users/Adi Surya/Documents/Documents/I Made Adi/UI/SEM 2/DDP 2/ProgFound2_Assignments/assignment-3/src/main/java");
															for (Registrasi regiseach : listRegistrasi){
																RegistrationWriter.writeJson(regiseach, directory);
															}
															loop1 = false;loop2= false;loop3=false;loop4=false;loop5=false;
														}
														else if(opsi2.equals("Y")){
															loop2 = false;loop3=false;loop4=false;loop5=false;
														}
														else if (opsi2.equals("#")){
															loop5 = false;
														}
													}
												}
												else if (opsi.equals("N")){
													loop4 = false;
												}
												else if (opsi.equals("#")){
													loop4 = false;
												}
											}
										}
									}
								}
								if (numAttraction.equals("#")){
									loop3 = false;
								}
							}
						}
						else{
							System.out.println("Unfortunately, no whale can perform any attraction, please choose other animals");
							
						}
					}
					else if (numAnimal.equals("#")){
						loop2 = false ;
					}
				}
			}
			else if (numSection.equals("2")){
				while(loop2){
					boolean loop3 = true;
					System.out.println("\n--World of Aves--\n1.Eagle\n2.Parrot\n");
					System.out.print("Please choose your preferred animals (type the number) :");
					String numAnimal = input.next();
					if (numAnimal.equals("1")){
						if(arrayEagle.size() > 0){
							while(loop3){
								boolean loop4 = true;
								String atraksiHewanStr = "";
								for (Attraction eachAttraction : atraksiEagle){
									atraksiHewanStr += (atraksiEagle.indexOf(eachAttraction)+1) + ". " + eachAttraction.getName() + "\n";
								}
								System.out.println("\n---Eagle---\nAttractions by Eagle:\n" + atraksiHewanStr);
								System.out.print("Please choose your preferred attraction (type the number) :");
								String numAttraction = input.next();
								for (Attraction eachAttraction : atraksiEagle){
									String indexAtraksi = String.valueOf((atraksiEagle.indexOf(eachAttraction)+1));
									if(numAttraction.equals(indexAtraksi)){
										Attraction atraksi = new Attraction(eachAttraction.getName(),eachAttraction.getType(),arrayEagle);
										while(loop4){
											boolean loop5 = true;
											System.out.print("Wow, one more step, please let us know your name: ");
											input.nextLine();
											name = input.nextLine();
											found:{
												for (Registrasi regiseach : listRegistrasi){
													if (name.equals(regiseach.getVisitorName())){
														listAtraksi = regiseach.addSelectedAttractions(atraksi);
														name = regiseach.getVisitorName();
														registrationId = regiseach.getRegistrationId();
														break found;
													}
												}
												registrationId += 1;
												listAtraksi = new ArrayList<SelectedAttraction>();
												listAtraksi.add(atraksi);
											}
											if(name.equals("#")){
												loop4 = false;
											}
											else{
												String namePerform = "";
												for (Animal performer : atraksi.getPerformers()){
													namePerform += performer.getName() + ",";
												}
												System.out.println("Yeay, final check!\nHere is your data, and the attraction you chose:"+
																   "\nName : " + name +
																   "\nAttractions : " + atraksi.getName() + " -> "+ atraksi.getType() +
																   "\nWith : " + namePerform);
												System.out.print("Is the data correct? (Y/N): ");
												String opsi = input.next();
												if (opsi.equals("Y")){
													while(loop5){
														System.out.print("Thank you for your interest. Would you like to register to other attractions? (Y/N): ");
														Registrasi regis = new Registrasi(registrationId,name,listAtraksi);
														found:{
															for (Registrasi regisBefore : listRegistrasi){
																if (regisBefore.getVisitorName().equals(regis.getVisitorName())){
																	listRegistrasi.remove(regisBefore);
																	listRegistrasi.add(regis);
																	break found;
																}
															}
															listRegistrasi.add(regis);
														}
														String opsi2 = input.next();
														if(opsi2.equals("N")){
															Path directory = Paths.get("C:/Users/Adi Surya/Documents/Documents/I Made Adi/UI/SEM 2/DDP 2/ProgFound2_Assignments/assignment-3/src/main/java");
															for (Registrasi regiseach : listRegistrasi){
																RegistrationWriter.writeJson(regiseach, directory);
															}
															loop1 = false;loop2= false;loop3=false;loop4=false;loop5=false;
														}
														else if(opsi2.equals("Y")){
															loop2 = false;loop3=false;loop4=false;loop5=false;
														}
														else if (opsi2.equals("#")){
															loop5 = false;
														}
													}
												}
												else if (opsi.equals("N")){
													loop4 = false;
												}
												else if (opsi.equals("#")){
													loop4 = false;
												}
											}
										}
									}
								}
								if (numAttraction.equals("#")){
									loop3 = false;
								}
							}
						}
						else{
							System.out.println("Unfortunately, no eagle can perform any attraction, please choose other animals");
							
						}
					}
					else if (numAnimal.equals("2")){
						if(arrayParrot.size() > 0){
							while(loop3){
								boolean loop4 = true;
								String atraksiHewanStr = "";
								for (Attraction eachAttraction : atraksiParrot){
									atraksiHewanStr += (atraksiParrot.indexOf(eachAttraction)+1) + ". " + eachAttraction.getName() + "\n";
								}
								System.out.println("\n---Parrot---\nAttractions by Parrot:\n" + atraksiHewanStr);
								System.out.print("Please choose your preferred attraction (type the number) :");
								String numAttraction = input.next();
								for (Attraction eachAttraction : atraksiParrot){
									String indexAtraksi = String.valueOf((atraksiParrot.indexOf(eachAttraction)+1));
									if(numAttraction.equals(indexAtraksi)){
										Attraction atraksi = new Attraction(eachAttraction.getName(),eachAttraction.getType(),arrayParrot);
										while(loop4){
											boolean loop5 = true;
											System.out.print("Wow, one more step, please let us know your name: ");
											input.nextLine();
											name = input.nextLine();
											found:{
												for (Registrasi regiseach : listRegistrasi){
													if (name.equals(regiseach.getVisitorName())){
														listAtraksi = regiseach.addSelectedAttractions(atraksi);
														name = regiseach.getVisitorName();
														registrationId = regiseach.getRegistrationId();
														break found;
													}
												}
												registrationId += 1;
												listAtraksi = new ArrayList<SelectedAttraction>();
												listAtraksi.add(atraksi);
											}
											if(name.equals("#")){
												loop4 = false;
											}
											else{
												String namePerform = "";
												for (Animal performer : atraksi.getPerformers()){
													namePerform += performer.getName() + ",";
												}
												System.out.println("Yeay, final check!\nHere is your data, and the attraction you chose:"+
																   "\nName : " + name +
																   "\nAttractions : " + atraksi.getName() + " -> "+ atraksi.getType() +
																   "\nWith : " + namePerform);
												System.out.print("Is the data correct? (Y/N): ");
												String opsi = input.next();
												if (opsi.equals("Y")){
													while(loop5){
														System.out.print("Thank you for your interest. Would you like to register to other attractions? (Y/N): ");
														Registrasi regis = new Registrasi(registrationId,name,listAtraksi);
														found:{
															for (Registrasi regisBefore : listRegistrasi){
																if (regisBefore.getVisitorName().equals(regis.getVisitorName())){
																	listRegistrasi.remove(regisBefore);
																	listRegistrasi.add(regis);
																	break found;
																}
															}
															listRegistrasi.add(regis);
														}
														String opsi2 = input.next();
														if(opsi2.equals("N")){
															Path directory = Paths.get("C:/Users/Adi Surya/Documents/Documents/I Made Adi/UI/SEM 2/DDP 2/ProgFound2_Assignments/assignment-3/src/main/java");
															for (Registrasi regiseach : listRegistrasi){
																RegistrationWriter.writeJson(regiseach, directory);
															}
															loop1 = false;loop2= false;loop3=false;loop4=false;loop5=false;
														}
														else if(opsi2.equals("Y")){
															loop2 = false;loop3=false;loop4=false;loop5=false;
														}
														else if (opsi2.equals("#")){
															loop5 = false;
														}
													}
												}
												else if (opsi.equals("N")){
													loop4 = false;
												}
												else if (opsi.equals("#")){
													loop4 = false;
												}
											}
										}
									}
								}
								if (numAttraction.equals("#")){
									loop3 = false;
								}
							}
						}
						else{
							System.out.println("Unfortunately, no parrot can perform any attraction, please choose other animals");
							
						}
					}
					else if(numAnimal.equals("#")){
						loop2 = false ;
					}		
				}
			}

			else if (numSection.equals("3")){
				while(loop2){
					boolean loop3 = true;
					System.out.println("\n--Reptillian Kingdom--\n1.Snake\n");
					System.out.print("Please choose your preferred section (type the number) :");
					String numAnimal = input.next();
					if (numAnimal.equals("1")){
						if (arraySnake.size() > 0){
							while (loop3){
								boolean loop4 = true;
								String atraksiHewanStr = "";
								for (Attraction eachAttraction : atraksiSnake){
									atraksiHewanStr += (atraksiSnake.indexOf(eachAttraction)+1) + ". " + eachAttraction.getName() + "\n";
								}
								System.out.println("\n---Snake---\nAttractions by Snake:\n" + atraksiHewanStr);
								System.out.print("Please choose your preferred animals (type the number) :");
								String numAttraction = input.next();
								for (Attraction eachAttraction : atraksiSnake){
									String indexAtraksi = String.valueOf((atraksiSnake.indexOf(eachAttraction)+1));
									if(numAttraction.equals(indexAtraksi)){
										Attraction atraksi = new Attraction(eachAttraction.getName(),eachAttraction.getType(),arraySnake);
										while(loop4){
											boolean loop5 = true;
											System.out.print("Wow, one more step, please let us know your name: ");
											input.nextLine();
											name = input.nextLine();
											found:{
												for (Registrasi regiseach : listRegistrasi){
													if (name.equals(regiseach.getVisitorName())){
														listAtraksi = regiseach.addSelectedAttractions(atraksi);
														name = regiseach.getVisitorName();
														registrationId = regiseach.getRegistrationId();
														break found;
													}
												}
												registrationId += 1;
												listAtraksi = new ArrayList<SelectedAttraction>();
												listAtraksi.add(atraksi);
											}
											if(name.equals("#")){
												loop4 = false;
											}
											else{
												String namePerform = "";
												for (Animal performer : atraksi.getPerformers()){
													namePerform += performer.getName() + ",";
												}
												System.out.println("Yeay, final check!\nHere is your data, and the attraction you chose:"+
																   "\nName : " + name +
																   "\nAttractions : " + atraksi.getName() + " -> "+ atraksi.getType() +
																   "\nWith : " + namePerform);
												System.out.print("Is the data correct? (Y/N): ");
												String opsi = input.next();
												if (opsi.equals("Y")){
													while(loop5){
														System.out.print("Thank you for your interest. Would you like to register to other attractions? (Y/N): ");
														Registrasi regis = new Registrasi(registrationId,name,listAtraksi);
														found:{
															for (Registrasi regisBefore : listRegistrasi){
																if (regisBefore.getVisitorName().equals(regis.getVisitorName())){
																	listRegistrasi.remove(regisBefore);
																	listRegistrasi.add(regis);
																	break found;
																}
															}
															listRegistrasi.add(regis);
														}
														String opsi2 = input.next();
														if(opsi2.equals("N")){
															Path directory = Paths.get("C:/Users/Adi Surya/Documents/Documents/I Made Adi/UI/SEM 2/DDP 2/ProgFound2_Assignments/assignment-3/src/main/java");
															for (Registrasi regiseach : listRegistrasi){
																RegistrationWriter.writeJson(regiseach, directory);
															}
															loop1 = false;loop2= false;loop3=false;loop4=false;loop5=false;
														}
														else if(opsi2.equals("Y")){
															loop2 = false;loop3=false;loop4=false;loop5=false;
														}
														else if (opsi2.equals("#")){
															loop5 = false;
														}
													}
												}
												else if (opsi.equals("N")){
													loop4 = false;
												}
												else if (opsi.equals("#")){
													loop4 = false;
												}
											}
										}
									}
								}
								if (numAttraction.equals("#")){
									loop3 = false;
								}
							}
						}
						else{
							System.out.println("Unfortunately, no snake can perform any attraction, please choose other animals");
							
						}
					}
					else if (numAnimal.equals("#")){
						loop2 = false ;
					}
				}
			}
			else if (numSection.equals("#")){
				loop1 = false ;
			}
		}
	}
}