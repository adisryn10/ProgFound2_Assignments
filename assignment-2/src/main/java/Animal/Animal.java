package Animal;

public class Animal{
	protected String nama;
	protected int length;
	protected String jenis;	//use in kandang class
	public Animal(String nama, int length,String jenis){
		this.nama = nama;
		this.length = length;
		this.jenis = jenis;
	}
	//getter each construction
	public String getNama(){
		return nama;
	}
	public int getLength(){
		return length;
	}
	public String getJenis(){
		return jenis;
	}
}