//Put class Parrot into package Animal
package Animal;
//import arraylist modul
import java.util.ArrayList;

//Make a Parrot class
public class Parrot extends Animal{
	public Parrot(String nama, int length){
		super(nama,length,"Parrot");
	}
	//getter for each constructor
	public String getNama(){
		return nama;
	}
	public int getLength(){
		return length;
	}
	public String getJenis(){
		return jenis;
	}
	//method order to fly
	public String orderToFly(){
		return "TERBAAAANG....";
	}
	//method do conversation
	public String doConversation(String word){
		return word;
	}
	//method to check the Parrot from arraylist
	public static boolean isCheckedParrot(String name, ArrayList<Animal> arrayParrot){
		for (int i = 0; i < arrayParrot.size(); i++){
			if (name.equals(arrayParrot.get(i).getNama())){
				return true;
			}
		}
		return false;
	}
}