//Put class Hamster into package Animal
package Animal;
//import arraylist modul
import java.util.ArrayList;

//Make a hamster class
public class Hamster extends Animal{
	public Hamster(String nama, int length){
		super(nama,length,"Hamster");
	}
	//method gnawing
	public String gnawing(){
		return "ngkkrit.. ngkkrrriiit";
	}
	//method runwheel
	public String runWheel(){
		return "trrr.... trrr....";
	}
	//method to check the Hamster from arraylist
	public static boolean isCheckedHamster(String name, ArrayList<Animal> arrayHamster){
		for (int i = 0; i < arrayHamster.size(); i++){
			if (name.equals(arrayHamster.get(i).getNama())){
				return true;
			}
		}
		return false;
	}
}