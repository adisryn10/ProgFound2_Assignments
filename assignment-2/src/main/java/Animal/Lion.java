//Put class Lion into package Animal
package Animal;
//import arraylist modul
import java.util.ArrayList;

//Make a Lion class
public class Lion extends Animal{
	public Lion(String nama, int length){
		super(nama,length,"Lion");
	}
	//getter for each constructor
	public String getNama(){
		return nama;
	}
	public int getLength(){
		return length;
	}
	public String getJenis(){
		return jenis;
	}
	public String hunt(){
		return "err...";
	}
	//method brushed
	public String brushed(){
		return "Hauhhmm!";
	}
	//method disturbed
	public String disturbed(){
		return "HAAHUM!!";
	}
	//method to check the Lion from arraylist
	public static boolean isCheckedLion(String name, ArrayList<Animal> arrayLion){
		for (int i = 0; i < arrayLion.size(); i++){
			if (name.equals(arrayLion.get(i).getNama())){
				return true;
			}
		}
		return false;
	}
}