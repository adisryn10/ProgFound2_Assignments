//Put class Cat into package animal
package Animal;
//import modul random and arraylist
import java.util.Random;
import java.util.ArrayList;

//make a class cat
public class Cat extends Animal{
	public Cat(String nama, int length){
		super(nama,length,"Cat");
	}
	//method brushed
	public String brushed(){
		return "Nyan....";
	}
	//method cuddle
	public String cuddle(){
		String[] arrayVoice = {"Miaaaw..", "Purrr..", "Mwaw!", "Mraaawr!"} ;
		Random generator = new Random();
		int randomIndex = generator.nextInt(arrayVoice.length);
		return (arrayVoice[randomIndex]);	
	}
	//method to check the cat from arraylist
	public static boolean isCheckedCat(String name, ArrayList<Animal> array){
		for (int i = 0; i < array.size(); i++){
			if (name.equals(array.get(i).getNama())){
				return true;
			}
		}
		return false;
	}
}