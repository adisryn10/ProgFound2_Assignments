//Put class Eagle into package Animal
package Animal;
//import arraylist modul
import java.util.ArrayList;

//Make a eagle class
public class Eagle extends Animal{
	public Eagle(String nama, int length){
		super(nama,length,"Eagle");
	}
	//Method order to fly
	public String orderToFly(){
		return "kwaakk...";
	}
	//method to check the eagle from arraylist
	public static boolean isCheckedEagle(String name, ArrayList<Animal> arrayEagle){
		for (int i = 0; i < arrayEagle.size(); i++){
			if (name.equals(arrayEagle.get(i).getNama())){
				return true;
			}
		}
		return false;
	}
}