//import module Kandang Animal Scanner and ArrayList
import Kandang.*;
import Animal.*;
import java.util.Scanner;
import java.util.ArrayList;

public class CareTaker{
	public static void main(String[] args){
		System.out.println("Welcome to Javari Park!");
		System.out.println("Input the number of animals");

		//Make variable input
		Scanner input = new Scanner(System.in);

		//Make arrayList of each Animal (for put in each level later)
		ArrayList <Animal> arrayListCat = new ArrayList <Animal>();
		ArrayList <Animal> arrayListLion = new ArrayList <Animal>();
		ArrayList <Animal> arrayListEagle = new ArrayList <Animal>();
		ArrayList <Animal> arrayListParrot = new ArrayList <Animal>();
		ArrayList <Animal> arrayListHamster = new ArrayList <Animal>();

		//input the number of cat
		System.out.print("cat: ");
		int numCat = input.nextInt();

		//check if there are cat(s) on the input
		if (numCat != 0){
			System.out.println("Provide the information of cat(s)");
			input.nextLine();
			String catInfo = input.nextLine();
			String[] catInfoArr = catInfo.split(",");
			//put each cat object in arrayList of Cat
			for (int i = 0; i < catInfoArr.length; i++){
				String[] catInfo2 = catInfoArr[i].split("\\|");
				String name = catInfo2[0];
				int length = Integer.parseInt(catInfo2[1]);
				Cat animalCat = new Cat(name,length);
				arrayListCat.add(animalCat);
			}
		}

		//input the number of lion
		System.out.print("lion: ");
		int numLion = input.nextInt();

		//check if there are lion(s) on the input
		if (numLion != 0){
			System.out.println("Provide the information of lion(s)");
			input.nextLine();
			String lionInfo = input.nextLine();
			String[] lionInfoArr = lionInfo.split(",");
			//put each lion object in arrayList of Lion
			for (int i = 0; i < lionInfoArr.length; i++){
				String[] lionInfo2 = lionInfoArr[i].split("\\|");
				String name = lionInfo2[0];
				int length = Integer.parseInt(lionInfo2[1]);
				Lion animalLion = new Lion(name,length);
				arrayListLion.add(animalLion);
			}
		}

		//input the number of eagle
		System.out.print("eagle: ");
		int numEagle = input.nextInt();
		//check if there are eagle(s) on the input
		if (numEagle != 0){
			System.out.println("Provide the information of eagle(s)");
			input.nextLine();
			String eagleInfo = input.nextLine();
			String[] eagleInfoArr = eagleInfo.split(",");
			//put each eagle object in arrayList of Lion
			for (int i = 0; i < eagleInfoArr.length; i++){
				String[] eagleInfo2 = eagleInfoArr[i].split("\\|");
				String name = eagleInfo2[0];
				int length = Integer.parseInt(eagleInfo2[1]);
				Eagle animalEagle = new Eagle(name,length);
				arrayListEagle.add(animalEagle);
			}
		}

		//input the number of parrot
		System.out.print("parrot: ");
		int numParrot = input.nextInt();
		//check if there are parrot(s) on the input
		if (numParrot != 0){
			System.out.println("Provide the information of parrot(s)");
			input.nextLine();
			String parrotInfo = input.nextLine();
			String[] parrotInfoArr = parrotInfo.split(",");
			//put each parrot object in arrayList of Lion
			for (int i = 0; i < parrotInfoArr.length; i++){
				String[] parrotInfo2 = parrotInfoArr[i].split("\\|");
				String name = parrotInfo2[0];
				int length = Integer.parseInt(parrotInfo2[1]);
				Parrot animalParrot = new Parrot(name,length);
				arrayListParrot.add(animalParrot);
			}
		}

		//input the number of hamster
		System.out.print("hamster: ");
		int numHamster = input.nextInt();
		//check if there are hamster(s) on the input
		if (numHamster != 0){
			System.out.println("Provide the information of hamster(s)");
			input.nextLine();
			String hamsterInfo = input.nextLine();
			String[] hamsterInfoArr = hamsterInfo.split(",");
			//put each hamster object in arrayList of Lion
			for (int i = 0; i < hamsterInfoArr.length; i++){
				String[] hamsterInfo2 = hamsterInfoArr[i].split("\\|");
				String name = hamsterInfo2[0];
				int length = Integer.parseInt(hamsterInfo2[1]);
				Hamster animalHamster = new Hamster(name,length);
				arrayListHamster.add(animalHamster);
			}
		}
		System.out.println("Animals has been successfully recorded!\n" + 
						   "=============================================");


		//Cage Arrangement
		System.out.println("Cage arrangement");

		//Check each arraylist if there are any object of each animal
		if (arrayListCat.size() > 0){
			System.out.println(ArrangeKandang.beforeArr(arrayListCat));		//Before Arrangement
			System.out.println(ArrangeKandang.Rearrange());					//After Arrangement
		}
		if (arrayListLion.size() > 0){
			System.out.println(ArrangeKandang.beforeArr(arrayListLion));
			System.out.println(ArrangeKandang.Rearrange());
		}
		if (arrayListEagle.size() > 0){
			System.out.println(ArrangeKandang.beforeArr(arrayListEagle));
			System.out.println(ArrangeKandang.Rearrange());
		}
		if (arrayListParrot.size() > 0){
			System.out.println(ArrangeKandang.beforeArr(arrayListParrot));
			System.out.println(ArrangeKandang.Rearrange());
		}
		if (arrayListHamster.size() > 0){
			System.out.println(ArrangeKandang.beforeArr(arrayListHamster));
			System.out.println(ArrangeKandang.Rearrange());
		}

		System.out.format("ANIMALS NUMBER:\ncat:%d\nlion:%d\nparrot:%d\neagle:%d\nhamster:%d\n\n",arrayListCat.size(),arrayListLion.size(),arrayListParrot.size(),arrayListEagle.size(),arrayListHamster.size());
		System.out.println("============================================= ");

		
		//Looping for being a caretaker of each animal
		while (true){
			System.out.println("Which animal you want to visit?\n"+
							  "(1: Cat, 2: Eagle, 3: Hamster, 4: Parrot, 5: Lion, 99:exit)" );

			//input number for desired animal you want to visit
			int checkNum = input.nextInt();

			//if number is 1 (Cat)
			if (checkNum == 1){
				System.out.print("Mention the name of cat you want to visit:");

				//input for the name of cat you want to visit
				String nameCat = input.next();
				for (Animal kucing : arrayListCat){

					//if the name of cat that you want to visit registered in the arraylist of cat
					if (kucing.getNama().equals(nameCat)){
						System.out.format("You are visiting %s (cat) now, what would you like to do?\n1: Brush the fur 2: Cuddle\n",nameCat);

						//Getting input for choose the things you want to do with the cat
						int catDo = input.nextInt();
						if (catDo == 1){
							System.out.format("Time to clean %s fur\n",nameCat);
							System.out.format("%s makes a voice %s\n",nameCat, ((Cat)kucing).brushed());
						}
						else if (catDo == 2){
							System.out.format("%s makes a voice %s\n",nameCat,((Cat)kucing).cuddle());
						}
						else{
							System.out.println("You do nothing..");
						}
						System.out.println("Back to the office!\n");
					}
				}
				//check if there is no cat in the arraylist of cat
				if (Cat.isCheckedCat(nameCat, arrayListCat) == false){
					System.out.println("There is no cat with that name! Back to the office!\n");
				}
			}

			//if number is 2 (Eagle)
			else if (checkNum == 2){
				System.out.print("Mention the name of eagle you want to visit:");

				//input for the name of eagle you want to visit
				String nameEagle = input.next();
				for (Animal elang : arrayListEagle){

					//if the name of eagle that you want to visit registered in the arraylist of eagle
					if (elang.getNama().equals(nameEagle)){
						System.out.format("You are visiting %s (eagle) now, what would you like to do?\n1: Order to fly\n",nameEagle);

						//Getting input for choose the things you want to do with the eagle
						int eagleDo = input.nextInt();
						if (eagleDo == 1){
							System.out.format("%s makes a voice: %s\n",nameEagle, ((Eagle)elang).orderToFly());
							System.out.println("You hurt!");
						}
						else{
							System.out.println("You do nothing...");
						}
						System.out.println("Back to the office!\n");
					}
				}
				//check if there is no eagle in the arraylist of eagle
				if (Eagle.isCheckedEagle(nameEagle, arrayListEagle) == false){
					System.out.println("There is no eagle with that name! Back to the office!\n");
				}
			}

			//if number is 3 (Hamster)
			else if (checkNum == 3){
				System.out.print("Mention the name of hamster you want to visit:");

				//input for the name of hamster you want to visit
				String nameHamster = input.next();
				for (Animal hams : arrayListHamster){

					//if the name of hamster that you want to visit registered in the arraylist of hamster
					if (hams.getNama().equals(nameHamster)){
						System.out.format("You are visiting %s (hamster) now, what would you like to do?\n1: See it gnawing 2: Order to run in the hamster wheel \n",nameHamster);
						
						//Getting input for choose the things you want to do with the hamster
						int hamsterDo = input.nextInt();
						if (hamsterDo == 1){
							System.out.format("%s makes a voice: %s\n",nameHamster,((Hamster)hams).gnawing());
						}
						else if (hamsterDo == 2){
							System.out.format("%s makes a voice: %s\n",nameHamster,((Hamster)hams).runWheel());
						}
						else{
							System.out.println("You do nothing...");
						}
						System.out.println("Back to the office!\n");
					}
				}
				//check if there is no hamster in the arraylist of hamster
				if (Hamster.isCheckedHamster(nameHamster,arrayListHamster) == false){
					System.out.println("There is no hamster with that name! Back to the office!\n");
				}
			}

			//if number is 4 (Parrot)
			else if (checkNum == 4){
				System.out.print("Mention the name of parrot you want to visit:");

				//input for the name of parrot you want to visit
				String nameParrot = input.next();
				for (Animal beo : arrayListParrot){

					//if the name of hamster that you want to visit registered in the arraylist of parrot
					if (beo.getNama().equals(nameParrot)){
						System.out.format("You are visiting %s (parrot) now, what would you like to do?\n1: Order to fly 2: Do conversation\n",nameParrot);
						
						//Getting input for choose the things you want to do with the parrot
						int parrotDo = input.nextInt();
						if (parrotDo == 1){
							System.out.println("Parrot Greeny flies!");
							System.out.format("%s makes a voice: %s\n",nameParrot,((Parrot)beo).orderToFly());
						}
						else if (parrotDo == 2){
							System.out.print("You say:");
							input.nextLine();
							String wordHuman = input.nextLine();
							System.out.format("%s says:%s\n",nameParrot,((Parrot)beo).doConversation(wordHuman).toUpperCase());
						}
						else{
							System.out.format("%s says HM?\n",nameParrot);
						}
						System.out.println("Back to the office!\n");
					}
				}

				//check if there is no parrot in the arraylist of parrot
				if (Parrot.isCheckedParrot(nameParrot,arrayListParrot) == false){
					System.out.println("There is no parrot with that name! Back to the office!\n");
				}
			}

			//if number is 5 (Lion)
			else if (checkNum == 5){
				System.out.print("Mention the name of lion you want to visit:");

				//input for the name of lion you want to visit
				String nameLion = input.next();
				for (Animal singa : arrayListLion){

					//if the name of lion that you want to visit registered in the arraylist of lion
					if (singa.getNama().equals(nameLion)){
						System.out.format("You are visiting %s (lion) now, what would you like to do?\n1: See it hunting 2: Brush the mane 3: Disturb it \n",nameLion);
						int lionDo = input.nextInt();

						//Getting input for choose the things you want to do with the lion
						if (lionDo == 1){
							System.out.println("Lion is hunting..");
							System.out.format("%s makes a voice %s\n", nameLion,((Lion)singa).hunt());
						}
						else if (lionDo == 2){
							System.out.println("Clean the lion's mane.. ");
							System.out.format("%s makes a voice %s\n", nameLion,((Lion)singa).brushed());
						}
						else if(lionDo == 3){
							System.out.format("%s makes a voice %s\n", nameLion,((Lion)singa).disturbed());
						}
						System.out.println("Back to the office!\n");
					}
				}
				//check if there is no lion in the arraylist of lion
				if (Lion.isCheckedLion(nameLion,arrayListLion) == false){
					System.out.println("There is no lion with that name! Back to the office!\n");
				}
			}

			//if number is 99 (Finished)
			else if (checkNum == 99){
				break;
			}
		}
	}
}