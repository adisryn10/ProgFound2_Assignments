//Put kandang in the package kandang
package Kandang;
//import Arraylist
import java.util.ArrayList;

public class Kandang{
	//class constructor for kandang class
	private String nama;
	private int length;
	private String hurufKandang;
	private String tipeKandang;

	//make a kandang function
	public Kandang(String nama, int length,String jenis){

		//check jenis of the animal (for pet animal)
		if (jenis.equals("Cat") || jenis.equals("Parrot") || jenis.equals("Hamster")){
			if (length < 45){
				this.hurufKandang = "A";
			} 
			else if (length >= 45 && length < 60){
				this.hurufKandang = "B";
			}
			else if (length >= 60){
				this.hurufKandang = "C";
			}
			this.tipeKandang = "indoor";
		}

		//check jenis of the animal (for wild animal)
		else if (jenis.equals("Lion") || jenis.equals("Eagle")){
			if (length < 75){
				this.hurufKandang = "A";
			}
			else if (length >= 75 && length < 90){
				this.hurufKandang = "B";
			}
			else if (length >= 90){
				this.hurufKandang = "C";
			}
			this.tipeKandang = "outdoor";
		}
	}
	//Getter tipe kandang (indoor or outdoor)
	public String getTipeKandang(){
		return tipeKandang;
	}
	//Getter huruf kandang (A or B or C)
	public String getHurufKandang(){
		return hurufKandang;
	}
}