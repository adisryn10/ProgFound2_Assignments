//Put arrange kandang class in package kandang
package Kandang;

//import module animal and arraylist
import Animal.*;
import java.util.ArrayList;

public class ArrangeKandang{
	private static Kandang kandang;
	private static ArrayList<String> level1;
	private static ArrayList<String> level2;
	private static ArrayList<String> level3;

	//Static method for before arrangement with arraylist of Animal parameter
	public static String beforeArr(ArrayList<Animal> arrayListAnimal){
		int panjangArrayAnimal = arrayListAnimal.size();
		//level 1, level 2 and level 3 for Animal arrangement
		level1 = new ArrayList<String>();
		level2 = new ArrayList<String>();
		level3 = new ArrayList<String>();
		//make the limit for level 1, level 2 and level 3
		int batasLevel1 = panjangArrayAnimal/3;			//limit for level 1
		int batasLevel2;								
		if (panjangArrayAnimal%3 == 2){					//limit for level 2 if arraylength mod 3 equals 2			
			batasLevel2 = (2*batasLevel1)+1;
		}
		else{
			batasLevel2 = 2*batasLevel1;				//limit for level 2 if arraylength mod 3 equals 0 or 1
		}
		//makes string format for each level
		String level1str = "level 1 : ";
		String level2str = "level 2 : ";
		String level3str = "level 3 : ";
		String location = "";

		if (batasLevel1 == 0){							//limit for each class(special for under 3 animals)
			batasLevel1 = 1;
			if (arrayListAnimal.size() == 1){
				batasLevel2 = 1;
			}
			else{
				batasLevel2 = 2;
			}
		}

		//looping for adding the cat string into each level
		for (int i = 0; i < batasLevel1; i ++){
			Animal hewan = arrayListAnimal.get(i);
			kandang = new Kandang(hewan.getNama(),hewan.getLength(),hewan.getJenis());
			String eachAnimalArr = String.format("%s(%d-%s)",hewan.getNama(),hewan.getLength(),kandang.getHurufKandang());	
			level1.add(eachAnimalArr);																							//Adding string of Animal into arraylist of level 1
			level1str += eachAnimalArr+",";																						//String format for each level
			location = kandang.getTipeKandang();																				//To determine the location
		}
		for (int i = batasLevel1 ; i < batasLevel2; i ++){
			Animal hewan = arrayListAnimal.get(i);
			kandang = new Kandang(hewan.getNama(),hewan.getLength(),hewan.getJenis());
			String eachAnimalArr = String.format("%s(%d-%s)",hewan.getNama(),hewan.getLength(),kandang.getHurufKandang());	
			level2.add(eachAnimalArr);
			level2str += eachAnimalArr+",";
		}
		for (int i = batasLevel2 ; i < panjangArrayAnimal; i++){
			Animal hewan = arrayListAnimal.get(i);
			kandang = new Kandang(hewan.getNama(),hewan.getLength(),hewan.getJenis());
			String eachAnimalArr = String.format("%s(%d-%s)",hewan.getNama(),hewan.getLength(),kandang.getHurufKandang());	
			level3.add(eachAnimalArr);
			level3str += eachAnimalArr+",";
		}
		return "location:" + location+ "\n"+ level3str+"\n"+level2str+"\n"+level1str + "\n";
	}

	//Make a rearrange method
	public static String Rearrange(){
		String level1str = "level 1 : ";
		String level2str = "level 2 : ";
		String level3str = "level 3 : ";

		//Looping for each level from the last object in each level
		//level 3 be level 1
		for (int i = level3.size();i > 0 ;i--){
			level1str += level3.get(i-1) + ",";
		}
		//level 1 be level 2
		for (int i = level1.size(); i > 0 ; i--){
			level2str += level1.get(i-1) + ",";
		}
		//level 2 be level 3
		for (int i = level2.size(); i > 0 ; i--){
			level3str += level2.get(i-1) + ",";
		}
		//formatting after arrangement
		return "After rearrangement...\n" + level3str+"\n"+level2str+"\n"+level1str + "\n";
		
	}
}