import java.util.*;

public class A1Station {
	//Class Atribute
    private static final double THRESHOLD = 250; // in kilograms	//to decide departure time
	private static TrainCar currentTrain = null;					//Object TrainCar
	private static int count = 0;									//To count BMI Average in Train Car
	private static double totalMassIndex;							//Variable to keep the result of Total BMI
	private static double totalWeight;								//Variable to keep the result of Total Weight

	//Make departure method
	public static void departure(){
		String category = "";
		double averagemass = totalMassIndex/count;					//Grouping the category of Average BMI
		if (averagemass < 18.5){
			category = "*underweight*";
		}
		else if (averagemass >= 18.5 && averagemass < 25){
			category = "*normal*";
		}
		else if (averagemass >= 25 && averagemass < 30){
			category = "*overweight*";
		}
		else if (averagemass >= 30){
			category = "*obese*";
		}
		String printmass = String.format("%.2f",averagemass);			//take 2 digit float number of Average BMI
		System.out.println( "The train departs to Javari Park");		//Printed format when The TrainCar depart
		System.out.print("[LOCO]<--");
		currentTrain.printCar();
		System.out.println("\nAverage mass index of all cats : " + printmass);
		System.out.println("In average, the cats in the train are : " + category);
    }
	
	//Run the program
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);						//make Scanner input
		String[] line = input.nextLine().split(" ");
		int num = Integer.parseInt(line[0]);						//Number of cat that will be on process
		
		
		for (int i = 0;i < num;i++){								//Looping based on the number inputed before
			String[] masukan = input.nextLine().split(",");			
			String nama = masukan[0];
			double weight = Double.parseDouble(masukan[1]);
			double length = Double.parseDouble(masukan[2]);
			WildCat cat = new WildCat(nama,weight,length);			//Make object cat from class WildCat 
			
			//Check if there is no traincar or the currettrain is already departed 
			if (currentTrain == null){
				count = 0;
				count += 1;
				currentTrain = new TrainCar(cat);						//Current train has the object
				totalMassIndex = currentTrain.computeTotalMassIndex();	
				totalWeight = currentTrain.computeTotalWeight();
				
			}
			//If the currenttrain has the object before
			else{
				count += 1;
				currentTrain = new TrainCar(cat,currentTrain);
				totalMassIndex = currentTrain.computeTotalMassIndex();
				totalWeight = currentTrain.computeTotalWeight();
			}
			// Check if all of the cat is already procced or the total weight is more than THRESHOLD
			if (i == (num-1) || currentTrain.computeTotalWeight() > THRESHOLD){
				departure();
				currentTrain = null;
			}
			
		}
    }
}
