public class WildCat {
    
	//Make a class atribute name weight and length
    public String name;
    public double weight; // In kilograms
    public double length; // In centimeters
	
	//Make wildcat class
    public WildCat(String name, double weight, double length) {
        this.name = name;
		this.weight = weight;
		this.length = length;
    }
	
	//Method compute BMI
    public double computeMassIndex() {
        double bmi = weight/((length/100)*(length/100));
		return bmi;
    }
}
