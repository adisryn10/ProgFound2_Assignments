public class TrainCar {
	
	//Class atribute
    public static final double EMPTY_WEIGHT = 20; // In kilograms (to count total weight)
	public WildCat cat;
	public TrainCar next;
	
	//Overloading constructor concept in Class TrainCar
    public TrainCar(WildCat cat) {
		this.cat = cat;
    }

    public TrainCar(WildCat cat, TrainCar next) {
		this.cat = cat;
		this.next = next;
    }
	
	//Method compute total weight in TrainCar Object
    public double computeTotalWeight() {
		if (this.next == null){						
			return cat.weight + EMPTY_WEIGHT;								//base case
		}
		else{
			return cat.weight + EMPTY_WEIGHT + next.computeTotalWeight();	//recursive case
		}
       
    }
	
	//Method compute total bmi in TrainCar Object
    public double computeTotalMassIndex() {
		if (this.next == null){						
			return cat.computeMassIndex();										//base case
		}
		else{
			return cat.computeMassIndex() + next.computeTotalMassIndex();		//recursive case
		}
    }

	//Method print Car in TrainCar object
    public void printCar() {
		if (this.next == null){
			System.out.print ("(" + cat.name + ")");		//base case
		}
		else{
			System.out.print( "(" + cat.name + ")--");
			next.printCar();								//recursive case
		}
    }
}
